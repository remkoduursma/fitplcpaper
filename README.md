# fitplc

This repository contains the code to generate the paper

*fitplc- an R package to fit curves describing vulnerability to cavitation* by Remko Duursma and Brendan Choat.

The analysis is written in R, the manuscript written in markdown and converted to a Word document with the `rmarkdown` package.

[The fitplc package is described here](https://www.bitbucket.org/remkoduursma/fitplc)
